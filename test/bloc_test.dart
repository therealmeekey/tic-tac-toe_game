import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:bloc/bloc.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart';

void main() {
  late XoxoBloc xoxoBloc;

  setUp(() {
    xoxoBloc = XoxoBloc();
  });

  tearDown(() {
    xoxoBloc.close();
  });

  void loadedOrClearGameTest({required XoxoEvent event}) {
    blocTest<XoxoBloc, XoxoState>(
      'привести состояние к стартовому значению',
      build: () => xoxoBloc,
      act: (bloc) => bloc.add(event),
      expect: () => [
        isA<XoxoPlayState>()
            .having((p) => p.msgValue, 'msgValue', 'Player 1’s Turn')
            .having((p) => p.tilesValue, 'tilesValue', List.filled(9, 0)),
      ],
    );
  }

  void playerClickEventTest() {
    blocTest<XoxoBloc, XoxoState>(
      'клик по полю',
      build: () => xoxoBloc,
      act: (bloc) => bloc
        ..add(LoadedGameEvent())
        ..add(PlayerClickEvent(index: 0)),
      expect: () => [
        anything,
        isA<XoxoState>()
            .having((p) => p.tiles, 'tiles', [1, 0, 0, 0, 0, 0, 0, 0, 0]),
      ],
    );
  }

  group('XoxoBloc:', () {
    for (final event in <XoxoEvent>[LoadedGameEvent(), ClearXoxoEvent()]) {
      group('При событии $event:', () => loadedOrClearGameTest(event: event));
    }
    group('При событии PlayerClickEvent:', () => playerClickEventTest());
  });
}
