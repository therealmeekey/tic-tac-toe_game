import 'dart:async';

import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';

import 'package:simple_game/features/game/domain/usecases/xoxo_utils.dart';

class XoxoCubitWrite {
  XoxoState _state = XoxoState();
  final _stateController = StreamController<XoxoState>.broadcast();

  Stream<XoxoState> get stream => _stateController.stream;

  XoxoState get state => _state;

  XoxoCubitWrite() {
    _initialize();
  }

  void _updateState(XoxoState state) {
    if (_state == state) return;
    _state = state;
    _stateController.add(state);
  }

  void _initialize() {
    _updateState(const XoxoInitial());
  }

  void loadGame() {
    _updateState(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: state.isme,
    ));
  }

  void clickPlayer({required int index}) {
    List<int> tile = state.tiles;
    if (tile[index] == 0) {
      if (state.isme) {
        tile[index] = 1;
      } else {
        tile[index] = 2;
      }
      _updateState(XoxoPlayState(
        msgValue: XoxoUtils().checkResult(!state.isme, tile),
        tilesValue: state.tiles,
        ismeStatus: !state.isme,
      ));
    }
  }

  void clear() {
    _updateState(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: true,
    ));
  }
}
