part of 'xoxo_bloc.dart';

abstract class XoxoEvent {
  const XoxoEvent();
}

class InitialXoXoEvent extends XoxoEvent {}
class LoadedGameEvent extends XoxoEvent {}

class PlayerClickEvent extends XoxoEvent {
  final int index;

  PlayerClickEvent({required this.index});
}

class ClearXoxoEvent extends XoxoEvent {}
