part of 'xoxo_bloc.dart';

class XoxoState extends Equatable {
  final List<int> tiles;
  final String msg;
  final bool isme;

  const XoxoState({
    this.tiles = const <int>[],
    this.msg = "",
    this.isme = true,
  });

  @override
  List<Object?> get props => [
        tiles,
        msg,
        isme,
      ];
}

class XoxoInitial extends XoxoState {
  const XoxoInitial();
}

class XoxoPlayState extends XoxoState {
  final List<int> tilesValue;
  final String msgValue;
  final bool ismeStatus;

  const XoxoPlayState({
    required this.tilesValue,
    required this.msgValue,
    required this.ismeStatus,
  }) : super(
          tiles: tilesValue,
          msg: msgValue,
          isme: ismeStatus,
        );
}
