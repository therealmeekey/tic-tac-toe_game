import 'dart:async';

import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';

import 'package:simple_game/features/game/domain/usecases/xoxo_utils.dart';

class XoxoBlocWrite {
  XoxoState _state = XoxoState();
  final _stateController = StreamController<XoxoEvent>.broadcast();
  late final Stream<XoxoState> _stateStream;

  Stream<XoxoState> get stream => _stateStream;

  XoxoState get state => _state;

  XoxoBlocWrite() {
    _stateStream = _stateController.stream
        .asyncExpand<XoxoState>(_mapEventToState)
        .asyncExpand(_updateState)
        .asBroadcastStream();
    dispatch(InitialXoXoEvent());
  }

  void dispatch(XoxoEvent event) {
    _stateController.add(event);
  }

  Stream<XoxoState> _updateState(XoxoState state) async* {
    if (_state == state) return;
    _state = state;
    yield state;
  }

  Stream<XoxoState> _mapEventToState(XoxoEvent event) async* {
    if (event is LoadedGameEvent) {
      yield XoxoPlayState(
        msgValue: XoxoUtils.initialMsg(),
        tilesValue: List.filled(9, 0),
        ismeStatus: state.isme,
      );
    } else if (event is PlayerClickEvent) {
      List<int> tile = state.tiles;
      if (tile[event.index] == 0) {
        if (state.isme) {
          tile[event.index] = 1;
        } else {
          tile[event.index] = 2;
        }
        yield XoxoPlayState(
          msgValue: XoxoUtils().checkResult(!state.isme, tile),
          tilesValue: state.tiles,
          ismeStatus: !state.isme,
        );
      }
    } else if (event is ClearXoxoEvent) {
      yield XoxoPlayState(
        msgValue: XoxoUtils.initialMsg(),
        tilesValue: List.filled(9, 0),
        ismeStatus: true,
      );
    } else {
      yield const XoxoInitial();
    }
  }
}
