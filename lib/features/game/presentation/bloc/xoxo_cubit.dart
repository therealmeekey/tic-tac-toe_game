import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';

import 'package:simple_game/features/game/domain/usecases/xoxo_utils.dart';

class XoxoCubit extends Cubit<XoxoState> {
  XoxoCubit() : super(const XoxoInitial());

  void loadGame() {
    emit(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: state.isme,
    ));
  }

  void clickPlayer({required int index}) {
    List<int> tile = state.tiles;
    if (tile[index] == 0) {
      if (state.isme) {
        tile[index] = 1;
      } else {
        tile[index] = 2;
      }
      emit(XoxoPlayState(
        msgValue: XoxoUtils().checkResult(!state.isme, tile),
        tilesValue: state.tiles,
        ismeStatus: !state.isme,
      ));
    }
  }

  void clear() {
    emit(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: true,
    ));
  }
}
