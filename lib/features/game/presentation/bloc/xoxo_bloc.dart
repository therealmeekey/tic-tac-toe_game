import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simple_game/features/game/domain/usecases/xoxo_utils.dart';

part 'xoxo_event.dart';

part 'xoxo_state.dart';

class XoxoBloc extends Bloc<XoxoEvent, XoxoState> {
  XoxoBloc() : super(const XoxoInitial()) {
    on<LoadedGameEvent>(loadGame);
    on<PlayerClickEvent>(clickPlayer);
    on<ClearXoxoEvent>(clear);
  }

  void loadGame(
    XoxoEvent event,
    Emitter<XoxoState> emit,
  ) {
    emit(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: state.isme,
    ));
  }

  void clickPlayer(
    PlayerClickEvent event,
    Emitter<XoxoState> emit,
  ) {
    List<int> tile = state.tiles;
    if (tile[event.index] == 0) {
      if (state.isme) {
        tile[event.index] = 1;
      } else {
        tile[event.index] = 2;
      }
      emit(XoxoPlayState(
        msgValue: XoxoUtils().checkResult(!state.isme, tile),
        tilesValue: state.tiles,
        ismeStatus: !state.isme,
      ));
    }
  }

  void clear(
    XoxoEvent event,
    Emitter<XoxoState> emit,
  ) {
    emit(XoxoPlayState(
      msgValue: XoxoUtils.initialMsg(),
      tilesValue: List.filled(9, 0),
      ismeStatus: true,
    ));
  }
}
