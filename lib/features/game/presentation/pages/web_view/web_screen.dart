import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:simple_game/features/game/data/datasources/constants/string_constants.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';

import 'package:simple_game/features/game/data/datasources/constants/asset_constatnts.dart';
import 'package:simple_game/features/game/presentation/widgets/gridview_widget.dart';

class WebScreen extends StatelessWidget {
  const WebScreen({
    Key? key,
    required this.tiles,
    required this.height,
    required this.width,
    required this.msg,
    required this.isMe,
  }) : super(key: key);

  final List<int>? tiles;
  final double height;
  final double width;
  final String msg;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(backGround),
          fit: BoxFit.cover,
        ),
      ),
      child: Row(
        children: [
          Flexible(
            flex: 2,
            child: GridViewWidget(tiles: tiles),
          ),
          Flexible(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Spacer(),
                const Spacer(),
                const Text(
                  "Крестики нолики",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const Spacer(),
                Text(
                  msg,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const Spacer(),
                OutlinedButton.icon(
                  icon: const Icon(
                    Icons.replay,
                    color: Colors.white,
                  ),
                  label: const Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Text(
                      reset,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  onPressed: () {
                    context.read<XoxoBloc>().add(ClearXoxoEvent());
                  },
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                    side: const BorderSide(width: 2.0, color: Colors.red),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(32.0),
                    ),
                  ),
                ),
                const Spacer(),
                const Spacer(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
