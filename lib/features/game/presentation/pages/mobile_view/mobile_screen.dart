import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_game/features/game/data/datasources/constants/asset_constatnts.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_cubit.dart';
import 'package:simple_game/features/game/presentation/widgets/gridview_widget.dart';

import 'package:simple_game/features/game/data/datasources/constants/string_constants.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';

class MobileScreen extends StatelessWidget {
  const MobileScreen({
    Key? key,
    required this.tiles,
    required this.height,
    required this.width,
    required this.msg,
    required this.isMe,
  }) : super(key: key);

  final List<int>? tiles;
  final double height;
  final double width;
  final String msg;
  final bool isMe;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(backGround),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          const Spacer(),
          const Text(
            "Крестики нолики",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Spacer(),
          Text(
            msg,
            style: const TextStyle(
              fontSize: 20,
              color: Colors.white,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Spacer(),
          GridViewWidget(tiles: tiles),
          const Spacer(),
          OutlinedButton.icon(
            icon: const Icon(
              Icons.replay,
              color: Colors.white,
            ),
            label: const Padding(
              padding: EdgeInsets.all(4.0),
              child: Text(
                reset,
                style: TextStyle(color: Colors.white),
              ),
            ),
            onPressed: () {
              context.read<XoxoBloc>().add(ClearXoxoEvent());
              // context.read<XoxoCubit>().clear();
            },
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.red,
              side: const BorderSide(width: 2.0, color: Colors.red),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(32.0),
              ),
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }
}
