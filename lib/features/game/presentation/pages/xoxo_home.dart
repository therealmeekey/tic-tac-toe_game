import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_bloc.dart';
import '../bloc/xoxo_cubit.dart';
import 'mobile_view/mobile_screen.dart';
import 'web_view/web_screen.dart';

class XoxoHome extends StatefulWidget {
  const XoxoHome({Key? key}) : super(key: key);

  @override
  State<XoxoHome> createState() => _XoxoHomeState();
}

class _XoxoHomeState extends State<XoxoHome> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'XoXo Game',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: BlocBuilder<XoxoBloc, XoxoState>(builder: (context, state) {
        // body: BlocBuilder<XoxoCubit, XoxoState>(builder: (context, state) {
          if (state is XoxoInitial) {
            context.read<XoxoBloc>().add(LoadedGameEvent());
            // context.read<XoxoCubit>().loadGame();
            return const Center(child: CircularProgressIndicator());
          } else {
            return BlocBuilder<XoxoBloc, XoxoState>(
            // return BlocBuilder<XoxoCubit, XoxoState>(
              builder: (context, state) {
                return LayoutBuilder(builder: (context, constraints) {
                  if (constraints.maxWidth < 600) {
                    return MobileScreen(
                      tiles: state.tiles,
                      height: constraints.maxHeight,
                      width: constraints.maxWidth,
                      msg: state.msg,
                      isMe: state.isme,
                    );
                  }
                  return WebScreen(
                    tiles: state.tiles,
                    height: constraints.maxHeight,
                    width: constraints.maxWidth,
                    msg: state.msg,
                    isMe: state.isme,
                  );
                });
              },
            );
          }
        }),
      ),
    );
  }
}
