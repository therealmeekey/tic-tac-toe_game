import 'package:flutter/material.dart';
import 'package:simple_game/features/game/presentation/bloc/xoxo_cubit.dart';
import 'package:simple_game/features/game/presentation/pages/xoxo_home.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'features/game/presentation/bloc/xoxo_bloc.dart';

void main() {
  runApp(
    // BlocProvider<XoxoCubit>(
    //   create: (context) => XoxoCubit(),
    BlocProvider<XoxoBloc>(
      create: (context) => XoxoBloc(),
      child: const XoxoHome(),
    ),
  );
}
